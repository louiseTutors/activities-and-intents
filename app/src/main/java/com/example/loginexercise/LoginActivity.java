package com.example.loginexercise;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    ArrayList<User> users = new ArrayList<>();
//    static User users[] = {
//            new User("Bruce Wayne", "batman", "iambatman"),
//            new User("Jon Snow", "kingnorth", "iknownothing")
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        users.add(new User("Bruce Wayne", "batman", "iambatman"));
        users.add(new User("Jon Snow", "kingnorth", "iknownothing"));
    }

    public void loginButtonPressed(View view) {
        EditText etUsername = findViewById(R.id.editUsername);
        EditText etPassword = findViewById(R.id.editPassword);

        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();

        Log.d(TAG, "loginButtonPressed: Login button was clicked!");
        Log.d(TAG, "loginButtonPressed: Username: " + username + " Password: " + password);

        User u = new User("", username, password);
        if (users.contains(u)) {

            int index = users.indexOf(u);
            String fullName = users.get(index).getFullName();

            // User and password is a match, switch to main activity
            Intent i = new Intent(this, MainActivity.class);
            i.putExtra("fullname", fullName);

            startActivity(i);

        } else {
            // No entries matching the username and password that was inputted.
            // Display an toast message showing the error.
            Toast.makeText(this, "Invalid username or password!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Back function is disabled for this app.", Toast.LENGTH_SHORT).show();
    }
}
