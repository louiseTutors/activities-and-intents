package com.example.loginexercise;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent i = getIntent();
        String name = i.getStringExtra("fullname");

        TextView txtName = findViewById(R.id.textWelcome);
        txtName.setText("Welcome, " + name + "!");
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Back function is disabled for this app.", Toast.LENGTH_SHORT).show();
    }

    public void logoutButtonPressed(View view) {
        Log.d(TAG, "logoutButtonPressed: Logout button was pressed");
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }
}
