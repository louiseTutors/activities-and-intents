package com.example.loginexercise;

import androidx.annotation.Nullable;

public class User {
    private String fullName;
    private String userName;
    private String password;

    public User(String fullName, String userName, String password) {
        this.fullName = fullName;
        this.userName = userName;
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof User) {
           User o = (User) obj;

           if (o.userName.equals(this.userName) && o.password.equals(this.password)) {
               return true;
           } else {
               return false;
           }
        }

        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.fullName != null ? this.fullName.hashCode() : 0);
        hash = 17 * hash + (this.userName != null ? this.userName.hashCode() : 0);
        hash = 17 * hash + (this.password != null ? this.password.hashCode() : 0);
        return hash;
    }
}
